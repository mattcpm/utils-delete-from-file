import datetime
import glob
import os
import subprocess
from send_mail import SendMail

# variables to set ###############################

BASE_PATH = "/home/sites/dev4.iview.co.uk/secure/python_delete_files_script/" # base path to directory
FILES = '' # prefix for csv files  to process
absolute_image_path_prefix = "/home/sites/dev4.iview.co.uk/" # add here missing path to image, I assume in DB we store only relative path, absolute path required here

# global vars ###############################

F_PATH = BASE_PATH +'raw/'
F_PROCESSED = BASE_PATH + 'processed/'

# email vars ###############################

FROM = 'delete_files@cpm-digital.com'
TO = ['matt.c@uk-cpm.com']
SUBJECT = 'Delete files Service'

def getBody(count, origin, sdate, errCount, errlog):
    return "Deleted " + count + " files from " + origin + " on " + sdate + ". /n Error count: "+ errCount +" /n ERROR LOG: /n/n"+ errlog 

def delete_files(file):
    global COUNT
    global ERROR
    global ERROR_LOG
    COUNT = 0
    ERROR = 0
    ERROR_LOG = ""

    fileToProcess = F_PATH  + file 
    try:
        file = open(fileToProcess, "r")
    except:
        print "unable to open file for processing: " + fileToProcess
    else:
        data = file.readlines()
        file.close()
        for line in data:
            try:
                file_to_delete = os.path.join(absolute_image_path_prefix, str(line).replace('"','').replace("'", ""))
                print file_to_delete
                os.system('rm -f ' + file_to_delete)
                COUNT += 1
            except Exception as e:
                ERROR += 1
                ERROR_LOG +=  str(e)
        print "Processed file: " + fileToProcess
        print "Succesfully deleted: " + str(COUNT)
        print "Error count: " + str(ERROR)
        if ERROR_LOG != "":
            print "Error Log :"
            print ERROR_LOG
        timeNow = datetime.datetime.now().strftime('%d-%m-%y')
        Message = getBody(str(COUNT), fileToProcess, timeNow, str(ERROR), ERROR_LOG)
        SendMail(FROM, TO, "Text", SUBJECT, Message)

def GetFiles(F_PATH, FILES):
    parsePattern = F_PATH + FILES + "*"
    globfiles = glob.glob(parsePattern)
    for file in globfiles:
        fileToProcess = (file.split('/')[-1]).rstrip()

        delete_files(fileToProcess)

        # os.rename(F_PATH+fileToProcess, F_PROCESSED+fileToProcess)
        

GetFiles(F_PATH, FILES)
