import datetime
import boto3

# global vars
# AWS config
AWS_REGION = 'eu-west-1'
ACCESS_ID = 'AKIAJK4F3VJKJPZNDSRA'
ACCESS_KEY = 'szYd8LvVfQR17VbGvSw6SB2V7M0rcclXPIakyEZ9'

# other variables
TIME_FORMAT = "%Y-%m-%d %H:%M:%S"

# SendMail (FROM, TO, TYPE, SUBJECT, BODY)
# variables
#
# FROM : string -> from email address
# TO : list -> to email address'
# TYPE : string -> Text | Html
# BODY : string -> email body
# SUBJECT : string -> email subject

def SendMail (FROM, TO, TYPE, SUBJECT, BODY):

    if ((TYPE == 'Text') or (TYPE == 'Html')) and type(TO) is list and (FROM != '' and SUBJECT != '' and BODY !=''):
        dateTimeNow = datetime.datetime.now().strftime(TIME_FORMAT)

        client = boto3.client('ses',region_name=AWS_REGION, aws_access_key_id=ACCESS_ID, aws_secret_access_key=ACCESS_KEY)

        response = client.send_email(
            Destination={
            'ToAddresses': TO,
            },
            Message={
                'Body': {
                    TYPE:{
                        'Data': BODY,
                        },
                },
                'Subject': {
                    'Data': SUBJECT,
                    },
                },
            Source = FROM,
        )
        return ("Email sent - {}".format(dateTimeNow))
    else:
        return ("Error - email not sent")

